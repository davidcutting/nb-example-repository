package uk.ac.uea.cmp.asciiArtist.grid.print;

import java.io.PrintWriter;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;
import uk.ac.uea.cmp.asciiArtist.grid.GridBoundsException;

public class FilePrint extends PrintMethod {

	@Override
	public void Print(Grid g) throws PrintException {
		String filename = getOption("filename");
		if (filename == null || filename.equalsIgnoreCase(""))
		{
			throw new PrintException("Filename Not present or set correctly");
		}
		
		PrintWriter output;
		
		try
		{
			output = new PrintWriter(filename, "UTF-8");
		}
		catch(Exception e)
		{
			throw new PrintException("Error opening file for writing: "+e.getMessage());
		}
		
		int x = g.getX();
		int y = g.getY();
		try
		{
			for (int yp = 0; yp < y; ++yp)
			{
				for (int xp = 0; xp < x; ++xp)
				{
					output.print(g.Get(xp, yp));
				}
				output.println("");
			}
			output.close();
		}
		catch(GridBoundsException gbe)
		{
			throw new PrintException("Grid exception during output: "+gbe.getMessage());
		}
		catch(Exception e)
		{
			throw new PrintException("Exception Occured During File Write: "+e.getMessage());
		}
	}

}
