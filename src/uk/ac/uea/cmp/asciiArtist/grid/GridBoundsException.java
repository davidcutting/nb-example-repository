package uk.ac.uea.cmp.asciiArtist.grid;

public class GridBoundsException extends Exception {
	public GridBoundsException(String message)
	{
		super(message);
	}
}
