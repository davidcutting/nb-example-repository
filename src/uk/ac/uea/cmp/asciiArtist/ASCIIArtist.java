package uk.ac.uea.cmp.asciiArtist;

import uk.ac.uea.cmp.asciiArtist.grid.print.FilePrint;
import uk.ac.uea.cmp.asciiArtist.grid.print.ScreenPrint;
import uk.ac.uea.cmp.asciiArtist.grid.print.PrintMethod;
import uk.ac.uea.cmp.asciiArtist.grid.*;
import uk.ac.uea.cmp.asciiArtist.shapes.*;

public class ASCIIArtist {

	public static void main(String[] args) throws Exception {
		System.out.println("asciiArtist: Starting");
		System.out.println("");
		
		Grid g = new Grid(30,30);
		
		
		// Drawing Elements
		CircleFactory.getInstance().Create(10).Draw(g, 15, 15);
		CircleFactory.getInstance().Create(10).Draw(g, 0, 0);
		CircleFactory.getInstance().Create(10).Draw(g, 10, 10);
		
		TriangleFactory.getInstance().Create(5).Draw(g, 25, 25);
		
		SquareFactory.getInstance().Create(3).Draw(g,5,25);
		
		// Printing Elements
		PrintMethod pm = new ScreenPrint();
		pm.setOption("showzero", "no");
		g.setPrintMethod(pm);
		
		g.Print();
		
		g.setPrintMethod(new FilePrint());
		g.getPrintMethod().setOption("filename", "output.txt");
		
		g.Print();
	}

}
