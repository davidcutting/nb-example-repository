package uk.ac.uea.cmp.asciiArtist.shapes;

import uk.ac.uea.cmp.asciiArtist.grid.Grid;

public class Square extends Shape {

	public Square(int r)
	{
		R = r;
		int x = r*2;
		int y = x;
		
		g = new Grid(x,y);
		
		
		for (int i=0; i<x; ++i)
		{
			try
			{
				g.Set(i, 0, 1);;
				g.Set(i, x-1, 1);
				g.Set(0, i, 1);
				g.Set(x-1, i, 1);
			}
			catch(Exception e)
			{
				//
			}
		}
	}
	
}
